#!/usr/bin/env python3


"""

""" """

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import pytest

from os import makedirs
from os.path import exists, join

from portage_hooks_manager.lib.hooksrepo import Dirs
from portage_hooks_manager.lib.hooksrepo import HooksRepo


class MockLocalRepo:

    def __init__(self, pth):

        self.repo_path = join(pth, "mock_local_repo")
        self.src = join(self.repo_path, "src")

        self.bashrc = join(self.src, "bashrc")
        self.bashrc_d = join(self.src, "bashrc.d")
        self.bashrc_d_script = join(self.bashrc_d, "script.sh")

    def prepare_repo(self):

        makedirs(self.bashrc_d)

        with open(self.bashrc, "w") as f:
            f.write("true")

        with open(self.bashrc_d_script, "w") as f:
            f.write("true")


@pytest.mark.parametrize("repo_file", [
    "",
    "src",
    "src/bashrc",
    "src/bashrc.d",
    "src/bashrc.d/script.sh"
])
def test_local_clone(tmpdir, repo_file):
    tmp = str(tmpdir)

    mr = MockLocalRepo(tmp)
    mr.prepare_repo()
    ds = Dirs(home=join(tmp, "home"))
    hr = HooksRepo(mr.repo_path, proto="local", dirs=ds)

    hr.ensure()
    assert exists(hr.with_repo(repo_file))


@pytest.mark.parametrize("script_dir,hr_file", [
    ("", "bashrc"),
    ("bashrc.d", "script.sh")
])
def test_local_install(tmpdir, script_dir, hr_file):
    tmp = str(tmpdir)

    mr = MockLocalRepo(tmp)
    mr.prepare_repo()
    ds = Dirs(home=join(tmp, "home"), portdir=join(tmp, "etc", "portage"))
    hr = HooksRepo(mr.repo_path, proto="local", dirs=ds)

    hr.ensure()
    hr.install_file(hr_file, script_dir=script_dir)
    assert exists(hr.with_repo())
