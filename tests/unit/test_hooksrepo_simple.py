#!/usr/bin/env python3


"""

""" """"

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import os
import pytest

from portage_hooks_manager.lib.hooksrepo import HooksRepo


def test_portdir():
    assert (HooksRepo("/").portdir == "/etc/portage")


def test_default_protocol():
    assert (HooksRepo("/").proto == "git")


@pytest.mark.parametrize("inp,exp", [
    ("git", "git"),
    ("local", "local")
])
def test_protocol(inp, exp):
    assert (HooksRepo("/", proto=inp).proto == exp)


def test_url():
    assert (HooksRepo("https://gitlab.com/src_prepare/somerepo").url
            == "https://gitlab.com/src_prepare/somerepo")


def test_dir_basename():
    assert (os.path.basename(
        HooksRepo("https://gitlab.com/src_prepare/somerepo").dir)
            == "https_gitlab.com_src_prepare_somerepo")
