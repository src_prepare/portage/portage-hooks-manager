#!/usr/bin/env python3


"""

""" """

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import pytest

from portage_hooks_manager.lib.url2dir import url2dir


@pytest.mark.parametrize("inp,exp", [
    ("", ""),
    ("z", "z"),
    ("/", "_"),
    ("/my/directory", "_my_directory"),
    ("https://gitlab.com/src_prepare/portage/portage-conf-goodies",
     "https_gitlab.com_src_prepare_portage_portage-conf-goodies"),
    ("git@gitlab.com:src_prepare/portage/portage-conf-goodies",
     "git@gitlab.com:src_prepare_portage_portage-conf-goodies"),
])
def test_url2dir(inp, exp):
    assert url2dir(inp) == exp
