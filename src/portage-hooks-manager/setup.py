#!/usr/bin/env python3


from setuptools import setup


try:
    from portage_hooks_manager import __version__
except ImportError:
    __version__ = "unknown"


setup(
    name="portage-hooks-manager",
    version=__version__,
    description="Portage Hooks Manager",
    author="Maciej Barć",
    author_email="xgqt@riseup.net",
    url="https://gitlab.com/src_prepare/portage/portage-conf-goodies",
    license="GPL-3",
    keywords="gentoo portage",
    python_requires=">=3.6.*",
    install_requires=["GitPython"],
    packages=["portage_hooks_manager", "portage_hooks_manager.lib"],
    include_package_data=True,
    zip_safe=False,
    entry_points={"console_scripts": [
        "portage-hooks-manager = portage_hooks_manager.main:main"
    ]},
)
