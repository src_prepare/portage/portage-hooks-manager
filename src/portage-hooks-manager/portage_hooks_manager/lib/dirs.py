#!/usr/bin/env python3


"""

""" """

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


from os import getenv, makedirs
from os.path import exists, join


class Dirs:
    """Collection of directory paths."""

    def __default_portdir():
        eprefix = getenv("EPREFIX")

        if eprefix is None:
            return "/etc/portage"
        else:
            return join(eprefix, "/etc/portage")

    def __init__(self, home=getenv("HOME"), portdir=__default_portdir()):

        self.portdir = portdir

        if home is None:
            self.phm_root = "/tmp"
        else:
            self.phm_root = home

        self.phm_cache = join(self.phm_root, ".cache", "portage-hooks-manager")
        self.phm_repos = join(self.phm_cache, "repos")

    def ensure_phm_repos(self) -> None:
        """Create phm_repos directory with parents."""

        if not exists(self.phm_repos):
            makedirs(self.phm_repos)
