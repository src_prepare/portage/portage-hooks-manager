#!/usr/bin/env python3


"""
Additional functions for HooksRepo meant for a CLI frontend.
""" """

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import os

from .hooksrepo import HooksRepo


def show(hr: HooksRepo) -> None:

    hr.ensure()

    print(">>> Hooks repository URL:")
    print(f"    {hr.url}")

    print(">>> Hooks repository main directory path:")
    print(f"    {hr.with_repo()}")

    if os.path.isfile(hr.bashrc):
        print(">>> The \"bashrc\" file exists.")

    for k, v in hr.sds_contents().items():
        print(f">>> Hook subdirecotry \"{k}\":")
        for f in v:
            print(f"    - {f}")
