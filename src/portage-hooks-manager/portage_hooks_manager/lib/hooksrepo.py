#!/usr/bin/env python3


"""

""" """

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
""" """

Example:
from portage_hooks_manager.lib.hooksrepo import HooksRepo
import os
p = os.path.join(os.path.abspath(os.path.curdir), "_tmp", "portage")
hr = HooksRepo(
"https://gitlab.com/src_prepare/portage/portage-conf-goodies", portdir=p)
hr.ensure()
hr.test()
hr.sds_contents()
os.makedirs(p)
hr.install_file("bashrc")
hr.install_file("ccache.sh", script_dir="bashrc.d")
"""


import os
import shutil

from git import Repo

from . import shellcheck
from .dirs import Dirs
from .url2dir import url2dir


def vprint(o: object, string: str) -> None:
    """Wrapper to print "string" if "o.verbose" is True."""

    if o.verbose:
        print(string)


class HooksRepo:
    """Class for manipulating Portage hooks repositories."""

    def __init__(self, url: str, proto="git", dirs=Dirs(), verbose=False):

        self.url = url
        self.proto = proto
        self.dirs = dirs
        self.verbose = verbose

        self.dir = os.path.join(dirs.phm_repos, url2dir(url))
        self.portdir = self.dirs.portdir

        self.bashrc = self.with_repo("src", "bashrc")
        self.bashrcd = self.with_repo("src", "bashrc.d")
        self.postsyncd = self.with_repo("src", "postsync.d")
        self.repopostsyncd = self.with_repo("src", "repo.postsync.d")
        self.script_dirs = [self.bashrcd, self.postsyncd, self.repopostsyncd]

    # -- Files --

    def with_repo(self, * pth: str) -> str:
        """Join hooks repository path with given strings."""

        return os.path.join(self.dir, * pth)

    def with_port(self, * pth: str) -> str:
        """Join portdir path with given strings."""

        return os.path.join(self.portdir, * pth)

    def clone(self) -> None:
        """Clone hooks repo."""

        vprint(self, f"[I] Cloning from {self.url} into {self.dir}...")

        {"git": (lambda o: Repo.clone_from(o.url, o.dir)),
         "local": (lambda o: shutil.copytree(o.url, o.dir))
         }[self.proto](self)

    def ensure(self) -> None:
        """Clone hooks repo if it does not exist."""

        self.dirs.ensure_phm_repos()

        if os.path.exists(self.dir):
            vprint(self, f"[I] Directory {self.dir} already exists")
        else:
            vprint(self, f"[I] Directory {self.dir} does not exist")
            self.clone()

    def update(self) -> None:
        """Update hooks repo."""

        if not os.path.exists(self.dir):
            self.clone()
        else:
            vprint(self, f"[I] Updating {self.dir}")
            orig = Repo(self.dir).remotes.origin
            orig.pull()

    def sds_contents(self) -> dict:
        """Returns a dictionary of script_dirs actually containing scripts."""

        m = map((lambda d: (os.path.basename(d), os.listdir(d))
                 if os.path.isdir(d) else False),
                self.script_dirs)
        return dict(filter((lambda v: v), m))

    # -- Test --

    def test_structure(self) -> bool:
        """True if hooks repo has correct structure."""

        self.ensure()
        return (self.sds_contents() != {} or os.path.exists(self.bashrc))

    def test_shellcheck(self) -> bool:
        """Test contained scripts using shellcheck."""

        ret = True

        for k, v in self.sds_contents().items():
            for f in v:
                if not shellcheck.test_file(self.with_repo("src", k, f)):
                    ret = False
                    break

        if not shellcheck.test_file(self.bashrc):
            ret = False

        return ret

    def test(self) -> bool:
        """Test hooks in the repo."""

        return (self.test_structure() and self.test_shellcheck())

    # -- Install --

    def install_file(self, hr_file: str, script_dir="") -> None:
        """Installs selected file to PORTDIR."""

        port_script_dir = os.path.join(self.portdir, script_dir)
        if not os.path.exists(port_script_dir):
            os.makedirs(port_script_dir)
        elif not os.path.isdir(port_script_dir):
            raise Exception(f"[E] Not a directory: {port_script_dir}")

        shutil.copy(self.with_repo("src", script_dir, hr_file),
                    os.path.join(port_script_dir, hr_file))

        vprint(self, f"[I] Installed {hr_file} into {port_script_dir}")

    def install_scripts(self, script_dir: str) -> None:
        """Install all scripts fond in target script directory."""

        for script in self.sds_contents()[script_dir]:
            self.install_file(script, script_dir=script_dir)

    def install_script_dirs(self) -> None:
        """Install all script directories."""

        for script_dir in self.sds_contents().keys():
            self.install_scripts(script_dir)

    def install_all(self) -> None:
        """Install all repository scripts (+ bashrc)."""

        self.install_script_dirs()
        if os.path.exists(self.bashrc):
            self.install_file("bashrc")

    # -- Remove --

    def remove_file(self, hr_file: str, script_dir="") -> None:
        """Remove selected file from PORTDIR."""

        f = os.path.join(script_dir, hr_file)
        rf = self.with_repo("src", f)
        pf = self.with_port(f)

        if not os.path.exists(rf):
            vprint(self, f"[W] File {f} not in hooks repository")
        elif not os.path.exists(pf):
            vprint(self, f"[W] File {f} not in portdir")
        else:
            os.remove(pf)
            vprint(self, f"[I] Removed {f} from {self.portdir}")

    def remove_scripts(self, script_dir: str) -> None:
        """Remove all scripts fond in target script directory."""

        for script in self.sds_contents()[script_dir]:
            self.remove_file(script, script_dir=script_dir)

    def remove_script_dirs(self) -> None:
        """Remove all script directories."""

        for script_dir in self.sds_contents().keys():
            self.remove_scripts(script_dir)

    def remove_all(self) -> None:
        """Remove all repository scripts (+ bashrc)."""

        self.remove_script_dirs()
        if os.path.exists(self.bashrc):
            self.remove_file("bashrc")
