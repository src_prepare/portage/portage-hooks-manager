#!/usr/bin/env python3


"""
Provides portage_hooks_manager main CLI entry point.
""" """

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import argparse

from . import __version__
from .lib.dirs import Dirs


class CLI:
    """Parse the command-line for "portage-hooks-manager"."""

    parser = argparse.ArgumentParser(
        epilog="""Copyright (c) 2022, src_prepare group,
        licensed under the GNU GPL v3 License"""
    )
    parser.add_argument(
        "-V", "--version",
        action="version",
        version=f"%(prog)s {__version__}"
    )
    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="increase verbosity"
    )
    parser.add_argument(
        "-u", "--url",
        type=str,
        default="https://gitlab.com/src_prepare/portage/portage-conf-goodies",
        help="hooks repo URL"
    )
    parser.add_argument(
        "-p", "--protocol",
        type=str, default="git",
        help="force a specified protocol to be used (git, local)"
    )
    parser.add_argument(
        "-a", "--action",
        type=str, default="show",
        help="action to be performed (update, show, install, remove)"
    )
    parser.add_argument(
        "-f", "--file",
        type=str, default="none",
        help="hooks repository files to use (all, *chosen*)"
    )
    parser.add_argument(
        "-d", "--dir",
        type=str, default="all",
        help="hooks repository script directories to use (all, none, *chosen*)"
    )
    parser.add_argument(
        "-P", "--portdir",
        type=str, default=Dirs().portdir,
        help="Portage configuration directory"
    )
    args = parser.parse_args()
