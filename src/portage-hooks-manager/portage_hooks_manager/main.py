#!/usr/bin/env python3


"""
Provides portage_hooks_manager main CLI entry point.
""" """

This file is part of portage-hooks-manager.

portage-hooks-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

portage-hooks-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022, src_prepare group
Original author: Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import sys

from .cli import CLI
from .lib import hr_frontend as HR_f
from .lib.dirs import Dirs
from .lib.hooksrepo import HooksRepo


def main():
    """Main."""

    args = CLI.args
    if args.verbose:
        print(f"[I] Arguments: {vars(args)}")

    dirs = Dirs(portdir=args.portdir)
    hooks_repo = HooksRepo(
        args.url, proto=args.protocol, dirs=dirs, verbose=args.verbose)

    real_script_dir = ("" if args.dir == "none" else args.dir)

    if args.action == "update":
        hooks_repo.update()

    elif args.action == "show":
        HR_f.show(hooks_repo)

    elif args.action == "test":
        if not hooks_repo.test():
            print("[E] Tests do not pass!")
            sys.exit(1)

    elif args.action == "install":
        if args.dir == "all":
            hooks_repo.install_all()
        elif args.file == "all":
            hooks_repo.install_scripts(args.dir)
        else:
            hooks_repo.install_file(args.file, script_dir=real_script_dir)

    elif args.action == "remove":
        if args.dir == "all":
            hooks_repo.remove_all()
        elif args.file == "all":
            hooks_repo.remove_scripts(args.dir)
        else:
            hooks_repo.remove_file(args.file, script_dir=real_script_dir)


if __name__ == "__main__":
    main()
