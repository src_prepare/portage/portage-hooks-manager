# This file is part of portage-hooks-manager.

# portage-hooks-manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# portage-hooks-manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with portage-hooks-manager.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2022, src_prepare group
# Original author: Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


MAKE            := make
PYTHON          := python3
PYTEST          := pytest
PIP             := pip
DOXYGEN         := doxygen
HELP2MAN        := help2man

SRCDIR          := $(PWD)/src
DOCSDIR         := $(PWD)/docs
DOCSDIR-MAN     := $(DOCSDIR)/artifacts/man
TESTSDIR        := $(PWD)/tests

PKGNAME         := portage-hooks-manager
PKGSRC          := $(SRCDIR)/$(PKGNAME)
REQUIREMENTS    := $(PWD)/requirements.txt
CLEAN-DIR-WHERE :=
CLEAN-DIR-TRASH :=

SETUPPY-TARGET  := build
SETUPPY-TFLAGS  :=
PIP-TARGET      :=
PIP-TFLAGS      :=


all: clean build


.PHONY: clean-dir pip setuppy

clean-dir:
	cd $(CLEAN-DIR-WHERE) && find . -name "$(CLEAN-DIR-TRASH)" -exec rm -r {} +

pip:
	$(PIP) $(PIP-TARGET) $(PIP-TFLAGS)

setuppy:
	cd $(PKGSRC) && $(PYTHON) ./setup.py $(SETUPPY-TARGET) $(SETUPPY-TFLAGS)


.PHONY: pip-install pip-remove pip-requirements

pip-install:
	$(MAKE) pip PIP-TARGET=install PIP-TFLAGS="--user -e $(PKGSRC)"

pip-remove:
	$(MAKE) pip PIP-TARGET=uninstall PIP-TFLAGS="--yes $(PKGNAME)"

pip-requirements:
	$(MAKE) pip PIP-TARGET=install PIP-TFLAGS="--user -r $(REQUIREMENTS)"


.PHONY: clean-build clean-dist clean-egg clean-pycache clean-pytest_cache

clean-build:
	$(MAKE) clean-dir CLEAN-DIR-WHERE=$(PKGSRC) CLEAN-DIR-TRASH="build"

clean-dist:
	$(MAKE) clean-dir CLEAN-DIR-WHERE=$(PKGSRC) CLEAN-DIR-TRASH="dist"

clean-docs:
	$(MAKE) clean-dir CLEAN-DIR-WHERE=$(DOCSDIR) CLEAN-DIR-TRASH="artifacts"

clean-egg:
	$(MAKE) clean-dir CLEAN-DIR-WHERE=$(PKGSRC) CLEAN-DIR-TRASH="*.egg*"

clean-pycache:
	$(MAKE) clean-dir CLEAN-DIR-WHERE=$(PKGSRC) CLEAN-DIR-TRASH="__pycache__"
	$(MAKE) clean-dir CLEAN-DIR-WHERE=$(TESTSDIR) CLEAN-DIR-TRASH="__pycache__"

clean-pytest_cache:
	$(MAKE) clean-dir CLEAN-DIR-WHERE=$(PWD) CLEAN-DIR-TRASH=".pytest_cache"


docs/artifacts/html:
	$(DOXYGEN) $(DOCSDIR)/doxygen.config

docs/artifacts/man:
	mkdir -p $(DOCSDIR-MAN)
	$(HELP2MAN) $(PWD)/scripts/$(PKGNAME) > $(DOCSDIR-MAN)/$(PKGNAME)


.PHONY: clean prepare build install docs test remove purge fresh ci

clean: clean-build clean-dist clean-docs
clean: clean-egg clean-pycache clean-pytest_cache

prepare: clean pip-requirements

build:
	$(MAKE) setuppy SETUPPY-TARGET=build SETUPPY-TFLAGS=

install: build
	$(MAKE) setuppy SETUPPY-TARGET=install SETUPPY-TFLAGS=--user

docs: docs/artifacts/html docs/artifacts/man

test:
	cd $(TESTSDIR) && $(PYTEST)

remove: pip-remove

purge: remove clean

fresh: purge prepare install

ci: fresh test
